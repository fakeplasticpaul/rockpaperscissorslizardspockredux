import expect from 'expect';
import {
    checkWinner,
    randomAiHand,
    player1SetHand,
    player2SetHand,
    playersDraw,
    player1win,
    player2win
} from '../actions/rounds';

describe('Check the AI random hand generator returns the right hands based on Math.random', () => {
    afterEach(() => {
        expect.restoreSpies();
    });

    it('Rock is returned when its meant to', () => {
        expect.spyOn(Math, 'random').andReturn(0.1);

        const result = randomAiHand();
        expect(result).toEqual('Rock');
    });

    it('Spock is returned when its meant to', () => {
        expect.spyOn(Math, 'random').andReturn(0.3);

        const result = randomAiHand();
        expect(result).toEqual('Spock');
    });

    it('Paper is returned when its meant to', () => {
        expect.spyOn(Math, 'random').andReturn(0.5);

        const result = randomAiHand();
        expect(result).toEqual('Paper');
    });

    it('Lizard is returned when its meant to', () => {
        expect.spyOn(Math, 'random').andReturn(0.7);

        const result = randomAiHand();
        expect(result).toEqual('Lizard');
    });

    it('Scissors is returned when its meant to', () => {
        expect.spyOn(Math, 'random').andReturn(0.9);

        const result = randomAiHand();
        expect(result).toEqual('Scissors');
    });
});

describe('Check winner function correctly declares the result', () => {
    it('Rock should beat scissors', () => {
        const result = checkWinner('Rock', 'Scissors');
        expect(result.outcome).toEqual('player1win');
    });

    it('Rock should beat Lizard', () => {
        const result = checkWinner('Rock', 'Lizard');
        expect(result.outcome).toEqual('player1win');
    });

    it('Rock should lose to Paper', () => {
        const result = checkWinner('Rock', 'Paper');
        expect(result.outcome).toEqual('player2win');
    });

    it('Rock should lose to Spock', () => {
        const result = checkWinner('Rock', 'Spock');
        expect(result.outcome).toEqual('player2win');
    });

    it('Rock should draw with Rock', () => {
        const result = checkWinner('Rock', 'Rock');
        expect(result.outcome).toEqual('draw');
    });

    it('Scissors should draw with scissors', () => {
        const result = checkWinner('Scissors', 'Scissors');
        expect(result.outcome).toEqual('draw');
    });

    it('Scissors should beat Lizard', () => {
        const result = checkWinner('Scissors', 'Lizard');
        expect(result.outcome).toEqual('player1win');
    });

    it('Scissors should beat Paper', () => {
        const result = checkWinner('Scissors', 'Paper');
        expect(result.outcome).toEqual('player1win');
    });

    it('Scissors should lose to Spock', () => {
        const result = checkWinner('Scissors', 'Spock');
        expect(result.outcome).toEqual('player2win');
    });

    it('Scissors should lost to Rock', () => {
        const result = checkWinner('Scissors', 'Rock');
        expect(result.outcome).toEqual('player2win');
    });

    it('Paper should lose to scissors', () => {
        const result = checkWinner('Paper', 'Scissors');
        expect(result.outcome).toEqual('player2win');
    });

    it('Paper should lose to Lizard', () => {
        const result = checkWinner('Paper', 'Lizard');
        expect(result.outcome).toEqual('player2win');
    });

    it('Paper should draw with Paper', () => {
        const result = checkWinner('Paper', 'Paper');
        expect(result.outcome).toEqual('draw');
    });

    it('Paper should beat Spock', () => {
        const result = checkWinner('Paper', 'Spock');
        expect(result.outcome).toEqual('player1win');
    });

    it('Paper should beat Rock', () => {
        const result = checkWinner('Paper', 'Rock');
        expect(result.outcome).toEqual('player1win');
    });

    it('Lizard should lose to scissors', () => {
        const result = checkWinner('Lizard', 'Scissors');
        expect(result.outcome).toEqual('player2win');
    });

    it('Lizard should draw with Lizard', () => {
        const result = checkWinner('Lizard', 'Lizard');
        expect(result.outcome).toEqual('draw');
    });

    it('Lizard should beat Paper', () => {
        const result = checkWinner('Lizard', 'Paper');
        expect(result.outcome).toEqual('player1win');
    });

    it('Lizard should beat Spock', () => {
        const result = checkWinner('Lizard', 'Spock');
        expect(result.outcome).toEqual('player1win');
    });

    it('Lizard should lost to Rock', () => {
        const result = checkWinner('Lizard', 'Rock');
        expect(result.outcome).toEqual('player2win');
    });

    it('Spock should beat scissors', () => {
        const result = checkWinner('Spock', 'Scissors');
        expect(result.outcome).toEqual('player1win');
    });

    it('Spock should lose to Lizard', () => {
        const result = checkWinner('Spock', 'Lizard');
        expect(result.outcome).toEqual('player2win');
    });

    it('Spock should lose to Paper', () => {
        const result = checkWinner('Spock', 'Paper');
        expect(result.outcome).toEqual('player2win');
    });

    it('Spock should draw with Spock', () => {
        const result = checkWinner('Spock', 'Spock');
        expect(result.outcome).toEqual('draw');
    });

    it('Spock should beat Rock', () => {
        const result = checkWinner('Spock', 'Rock');
        expect(result.outcome).toEqual('player1win');
    });
});

describe('Check player1SetHand action', () => {
    it(' returns the correct json when Rock is passed in', () => {
        const result = player1SetHand('Rock');
        expect(result).toEqual({
            type: 'PLAYER1_SET_HAND',
            hand: 'Rock'
        });
    });

    it(' returns the correct json when Test is passed in', () => {
        const result = player1SetHand('Test');
        expect(result).toEqual({
            type: 'PLAYER1_SET_HAND',
            hand: 'Test'
        });
    });
});

describe('Check player2SetHand action', () => {
    it(' returns the correct json when Rock is passed in', () => {
        const result = player2SetHand('Rock');
        expect(result).toEqual({
            type: 'PLAYER2_SET_HAND',
            hand: 'Rock'
        });
    });

    it(' returns the correct json when Test is passed in', () => {
        const result = player2SetHand('Test');
        expect(result).toEqual({
            type: 'PLAYER2_SET_HAND',
            hand: 'Test'
        });
    });
});

describe('Test the result actions return correctly', () => {
    it('Result draw action', () => {
        const result = playersDraw({ 1: 2, 2: 3 });
        expect(result).toEqual({
            type: 'PLAYERS_DRAW',
            result: { 1: 2, 2: 3 }
        });
    });

    it('Player 1 win action', () => {
        const result = player1win({ 1: 2, 2: 3 });
        expect(result).toEqual({
            type: 'PLAYER1_WIN',
            result: { 1: 2, 2: 3 }
        });
    });

    it('Player 2 win action', () => {
        const result = player2win({ 1: 2, 2: 3 });
        expect(result).toEqual({
            type: 'PLAYER2_WIN',
            result: { 1: 2, 2: 3 }
        });
    });
});

describe('Play the round action', () => {

});
