/* eslint-disable react/jsx-filename-extension */

import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import History from '../components/History';

describe('<History />', () => {
    it('The list is not rendered when visible is false', () => {
        const component = shallow(
            <History history={[]} />
        );
        expect(component.find('.history').length).toEqual(0);
    });

    it('The list is rendered when visible is true', () => {
        const component = shallow(
            <History history={[]} visible />
        );
        expect(component.find('.history').length).toEqual(1);
    });

    it('The show history button is not rendered if the list 0 in length ', () => {
        const component = shallow(
            <History history={[]} visible />
        );
        expect(component.find('.button-link').length).toEqual(0);
    });

    it('The show history button is not rendered if the list 1 in length ', () => {
        const component = shallow(
            <History
                history={[{ player1hand: 'Rock', player2hand: 'Paper', outcome: 'test' }]}
                visible
            />
        );
        expect(component.find('.button-link').length).toEqual(0);
    });

    it('The show history button is rendered if the list > 1 in length ', () => {
        const component = shallow(
            <History
                history={[
                    { player1hand: 'Rock', player2hand: 'Paper', outcome: 'test' },
                    { player1hand: 'Rock', player2hand: 'Paper', outcome: 'test2' }
                ]}
                visible
            />
        );
        expect(component.find('.button-link').length).toEqual(1);
    });

    it('The outcome and Hands are correctly rendered and only the first is rendered', () => {
        const component = shallow(
            <History
                history={[
                    { player1hand: 'Rock', player2hand: 'Paper', outcome: 'test' },
                    { player1hand: 'Rock', player2hand: 'Paper', outcome: 'test2' }
                ]}
                visible
            />
        );

        expect(component.find('.history-row').length).toEqual(1);
        expect(component.find('.history-row').at(0).html()).toContain(
            '<li class="history-row">1:' +
            '<svg width="20" height="20" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Rock">');
    });

    it('The outcome and Hands are correctly rendered and the first 2 are rendered' +
        'in reverse', () => {
        const component = shallow(
            <History
                history={[
                    { player1hand: 'Rock', player2hand: 'Paper', outcome: 'test' },
                    { player1hand: 'Scissors', player2hand: 'Paper', outcome: 'test2' },
                    { player1hand: 'Scissors', player2hand: 'Paper', outcome: 'test2' }
                ]}
                visible
            />
        );
        expect(component.find('.history-row').at(0).html()).toContain(
            '<li class="history-row">' +
            '2:<svg width="20" height="20" viewBox="0 0 1792 1792"' +
            ' role="img" aria-label="Icon representing Scissors">');
        expect(component.find('.history-row').at(1).html()).toContain(
            '<li class="history-row">1:' +
            '<svg width="20" height="20" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Rock">');
    });
});
