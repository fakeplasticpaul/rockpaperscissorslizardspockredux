import expect from 'expect';
import deepFreeze from 'deep-freeze';
import reducer from '../reducers';
import {
    player1SetHand,
    player2SetHand,
    playersDraw,
    player1win,
    player2win
} from '../actions/rounds';
import toggleHistory from '../actions/game';

describe('reducer', () => {
    afterEach(() => {
        expect.restoreSpies();
    });

    it('should provide the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        });
    });

    it('should handle PLAYER1_SET_HAND action', () => {
        const stateBefore = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = player1SetHand('Rock');
        const stateAfter = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {
                player1: 'Rock'
            },
            score: {
                player1: 0,
                player2: 0
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });

    it('should handle PLAYER2_SET_HAND action', () => {
        const stateBefore = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = player2SetHand('Paper');
        const stateAfter = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {
                player2: 'Paper'
            },
            score: {
                player1: 0,
                player2: 0
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });

    it('should handle PLAYERS_DRAW action', () => {
        const stateBefore = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = playersDraw({ outcome: 'draw', player1: 'Rock', player2: 'Rock' });
        const stateAfter = {
            game: {
                history: false
            },
            history: [{ outcome: 'draw', player1: 'Rock', player2: 'Rock' }],
            i18n: {},
            rounds: {
                result: 'draw'
            },
            score: {
                player1: 1,
                player2: 1
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });

    it('should handle PLAYER1_WIN action', () => {
        const stateBefore = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = player1win({ outcome: 'player1win', player1: 'Rock', player2: 'Rock' });
        const stateAfter = {
            game: {
                history: false
            },
            history: [{ outcome: 'player1win', player1: 'Rock', player2: 'Rock' }],
            i18n: {},
            rounds: {
                result: 'player1win'
            },
            score: {
                player1: 1,
                player2: 0
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });

    it('should handle PLAYER2_WIN action', () => {
        const stateBefore = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = player2win({ outcome: 'player2win', player1: 'Rock', player2: 'Rock' });
        const stateAfter = {
            game: {
                history: false
            },
            history: [{ outcome: 'player2win', player1: 'Rock', player2: 'Rock' }],
            i18n: {},
            rounds: {
                result: 'player2win'
            },
            score: {
                player1: 0,
                player2: 1
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });

    it('should handle TOGGLE_HISTORY action', () => {
        const stateBefore = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = toggleHistory();
        const stateAfter = {
            game: {
                history: true
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });

    it('should handle TOGGLE_HISTORY action in reverse', () => {
        const stateBefore = {
            game: {
                history: true
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        const action = toggleHistory();
        const stateAfter = {
            game: {
                history: false
            },
            history: [],
            i18n: {},
            rounds: {},
            score: {
                player1: 0,
                player2: 0
            }
        };

        deepFreeze(stateBefore);
        deepFreeze(action);

        expect(reducer(stateBefore, action)).toEqual(stateAfter);
    });
});
