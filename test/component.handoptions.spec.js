/* eslint-disable react/jsx-filename-extension */

import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import HandOptions from '../components/HandOptions';

describe('<HandOptions />', () => {
    it('Check the correct amount of buttons are rendered', () => {
        const component = shallow(
            <HandOptions />
        );
        expect(component.find('button').length).toEqual(5);
    });

    it('Check the first button text is correct', () => {
        const component = shallow(
            <HandOptions />
        );
        expect(component.find('button .hide-visually').at(0).text()).toEqual('Rock');
    });

    it('Check the first button dispacthes correctly', () => {
        const dispatchSpy = expect.createSpy();
        const component = shallow(
            <HandOptions dispatch={dispatchSpy} />
        );
        component.find('button').at(0).simulate('click');
        expect(dispatchSpy).toHaveBeenCalled();
    });

    it('Check the second button text is correct', () => {
        const component = shallow(
            <HandOptions />
        );
        expect(component.find('button .hide-visually').at(1).text()).toEqual('Paper');
    });

    it('Check the second button dispacthes correctly', () => {
        const dispatchSpy = expect.createSpy();
        const component = shallow(
            <HandOptions dispatch={dispatchSpy} />
        );
        component.find('button').at(1).simulate('click');
        expect(dispatchSpy).toHaveBeenCalled();
    });

    it('Check the third button text is correct', () => {
        const component = shallow(
            <HandOptions />
        );
        expect(component.find('button .hide-visually').at(2).text()).toEqual('Scissors');
    });

    it('Check the third button dispacthes correctly', () => {
        const dispatchSpy = expect.createSpy();
        const component = shallow(
            <HandOptions dispatch={dispatchSpy} />
        );
        component.find('button').at(2).simulate('click');
        expect(dispatchSpy).toHaveBeenCalled();
    });

    it('Check the fourth button text is correct', () => {
        const component = shallow(
            <HandOptions />
        );
        expect(component.find('button .hide-visually').at(3).text()).toEqual('Lizard');
    });

    it('Check the fourth button dispacthes correctly', () => {
        const dispatchSpy = expect.createSpy();
        const component = shallow(
            <HandOptions dispatch={dispatchSpy} />
        );
        component.find('button').at(3).simulate('click');
        expect(dispatchSpy).toHaveBeenCalled();
    });

    it('Check the fifth button text is correct', () => {
        const component = shallow(
            <HandOptions />
        );
        expect(component.find('button .hide-visually').at(4).text()).toEqual('Spock');
    });

    it('Check the fifth button dispacthes correctly', () => {
        const dispatchSpy = expect.createSpy();
        const component = shallow(
            <HandOptions dispatch={dispatchSpy} />
        );
        component.find('button').at(4).simulate('click');
        expect(dispatchSpy).toHaveBeenCalled();
    });
});
