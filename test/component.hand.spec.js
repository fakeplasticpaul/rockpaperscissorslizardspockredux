/* eslint-disable react/jsx-filename-extension */

import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import Hand from '../components/Hand';

describe('<Hand />', () => {
    it('Check renders the rock icon correctly', () => {
        const component = shallow(
            <Hand height="100" width="100" type="Rock" />
        );
        expect(component.find('svg').html()).toInclude('' +
            '<svg width="100" height="100" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Rock">');
    });

    it('Check renders the paper icon correctly', () => {
        const component = shallow(
            <Hand height="200" width="100" type="Paper" />
        );
        expect(component.find('svg').html()).toInclude('' +
            '<svg width="100" height="200" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Paper">');
    });

    it('Check renders the scissors icon correctly', () => {
        const component = shallow(
            <Hand height="100" width="300" type="Scissors" />
        );
        expect(component.find('svg').html()).toInclude('' +
            '<svg width="300" height="100" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Scissors">');
    });

    it('Check renders the lizard icon correctly', () => {
        const component = shallow(
            <Hand height="100" width="100" type="Lizard" />
        );
        expect(component.find('svg').html()).toInclude('' +
            '<svg width="100" height="100" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Lizard">');
    });

    it('Check renders the spock icon correctly', () => {
        const component = shallow(
            <Hand height="100" width="100" type="Spock" />
        );
        expect(component.find('svg').html()).toInclude('' +
            '<svg width="100" height="100" viewBox="0 0 1792 1792" role="img" ' +
            'aria-label="Icon representing Spock">');
    });
});
