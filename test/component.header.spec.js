/* eslint-disable react/jsx-filename-extension */

import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import Header from '../components/Header';

describe('<Header />', () => {
    it('Renders correctly', () => {
        const component = shallow(
            <Header round="1" player1Score="2" player2Score="3" />
        );
        expect(component.find('header').html()).toInclude(
            '<header class="header">' +
            '<h1 class="hide-visually">Title</h1>' +
            'Round 1' +
            '<div class="header-score1">Player1title:<span class="txt-emphasize"> 2</span></div>' +
            '<div class="header-score2">Player2title:<span class="txt-emphasize"> 3</span>' +
            '</div>' +
            '</header>');
    });

    it('Renders correctly with different values', () => {
        const component = shallow(
            <Header round="4" player1Score="5" player2Score="6" />
        );
        expect(component.find('header').html()).toInclude(
            '<header class="header">' +
            '<h1 class="hide-visually">Title</h1>' +
            'Round 4' +
            '<div class="header-score1">Player1title:<span class="txt-emphasize"> 5</span></div>' +
            '<div class="header-score2">Player2title:<span class="txt-emphasize"> 6</span>' +
            '</div>' +
            '</header>');
    });
});
