import expect from 'expect';
import toggleHistory from '../actions/game';

describe('Check the game actions respond correctly', () => {
    it('Check the response for toggle history', () => {
        const result = toggleHistory();
        expect(result).toEqual({
            type: 'TOGGLE_HISTORY'
        });
    });
});
