/* eslint-disable react/jsx-filename-extension */

import expect from 'expect';
import React from 'react';
import { shallow } from 'enzyme';
import Result from '../components/Result';

describe('<Result />', () => {
    it('Check the h2 is populated correctly on player1 win', () => {
        const component = shallow(
            <Result result="player1win" player1hand="Rock" player2hand="Scissors" />
        );
        expect(component.find('h2').html()).toInclude('<h2>Rock Scissors</h2>');
    });

    it('Check the h2 is populated correctly on player2 win', () => {
        const component = shallow(
            <Result result="player2win" player1hand="Rock" player2hand="Scissors" />
        );
        expect(component.find('h2').html()).toInclude('<h2>Scissors Rock</h2>');
    });

    it('Check the h2 is populated correctly on draw', () => {
        const component = shallow(
            <Result result="draw" />
        );
        expect(component.find('h2').html()).toInclude('<h2>No winner this time</h2>');
    });

    it('Check nothing is rendered if no result is passed in', () => {
        const component = shallow(
            <Result player1hand="Rock" player2hand="Paper" />
        );
        expect(component.find('section').length).toEqual(0);
    });
});
