const initialState = {
    player1: 0,
    player2: 0
};
const score = (state = initialState, action) => {
    const currentPlayer1Score = state.player1;
    const currentPlayer2Score = state.player2;

    switch (action.type) {
        case 'PLAYERS_DRAW':
            return Object.assign({}, state, {
                player1: currentPlayer1Score + 1,
                player2: currentPlayer2Score + 1
            });
        case 'PLAYER1_WIN':
            return Object.assign({}, state, {
                player1: currentPlayer1Score + 1
            });

        case 'PLAYER2_WIN':
            return Object.assign({}, state, {
                player2: currentPlayer2Score + 1
            });

        default:
            return state;
    }
};

export default score;
