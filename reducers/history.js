const history = (state = [], action) => {
    switch (action.type) {

        case 'PLAYERS_DRAW':
        case 'PLAYER1_WIN':
        case 'PLAYER2_WIN':
            return [...state, action.result];
        default:
            return state;
    }
};

export default history;
