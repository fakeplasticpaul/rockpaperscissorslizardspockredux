const rounds = (state = {}, action) => {
    switch (action.type) {
        case 'PLAYER1_SET_HAND':
            return Object.assign({}, state, {
                player1: action.hand
            });
        case 'PLAYER2_SET_HAND':
            return Object.assign({}, state, {
                player2: action.hand
            });

        case 'PLAYERS_DRAW':
            return Object.assign({}, state, {
                result: 'draw'
            });

        case 'PLAYER1_WIN':
            return Object.assign({}, state, {
                result: 'player1win'
            });

        case 'PLAYER2_WIN':
            return Object.assign({}, state, {
                result: 'player2win'
            });

        default:
            return state;
    }
};

export default rounds;
