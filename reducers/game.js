const initialState = {
    history: false
};

const game = (state = initialState, action) => {
    switch (action.type) {
        case 'TOGGLE_HISTORY':
            return Object.assign({}, state, {
                history: !state.history
            });

        default:
            return state;
    }
};

export default game;
