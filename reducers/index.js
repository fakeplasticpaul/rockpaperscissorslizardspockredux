import { combineReducers } from 'redux';
import { i18nReducer } from 'react-redux-i18n';
import game from './game';
import rounds from './rounds';
import score from './score';
import history from './history';

const rockPaperScissorsReducers = combineReducers({
    i18n: i18nReducer,
    game,
    rounds,
    score,
    history
});

export default rockPaperScissorsReducers;
