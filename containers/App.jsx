import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Header from '../components/Header';
import HandOptions from '../components/HandOptions';
import Result from '../components/Result';
import History from '../components/History';

require('../assets/app.css');

const App = ({ dispatch, game, rounds, score, history }) => {
    return (
        <main>
            <Header
                round={history.length + 1}
                player1Score={score.player1}
                player2Score={score.player2}
            />

            <section className="board">

                <HandOptions dispatch={dispatch} />

                <Result
                    result={rounds.result}
                    player1hand={rounds.player1}
                    player2hand={rounds.player2}
                />

                <History visible={game.history} history={history} dispatch={dispatch} />

            </section>
        </main>
    );
};

App.propTypes = {
    game: PropTypes.object.isRequired,
    score: PropTypes.object.isRequired,
    history: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
    rounds: PropTypes.object.isRequired
};

/* eslint-disable arrow-body-style */
const ConnectedApp = connect(
    state => ({
        game: state.game,
        rounds: state.rounds,
        score: state.score,
        history: state.history
    })
)(App);
/* eslint-enable arrow-body-style */

export default ConnectedApp;

