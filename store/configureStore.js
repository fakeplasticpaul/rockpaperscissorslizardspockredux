import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {
    loadTranslations,
    setLocale,
    syncTranslationWithStore,
} from 'react-redux-i18n';
import reducer from '../reducers';
import translationsObject from '../i18n/i18n';

export default function configureStore() {
    const store = createStore(reducer,
        window.devToolsExtension && window.devToolsExtension(),
        applyMiddleware(thunk)
    );

    syncTranslationWithStore(store);
    store.dispatch(loadTranslations(translationsObject));
    store.dispatch(setLocale('en'));

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextReducer = require('../reducers').default;

            store.replaceReducer(nextReducer);
        });
    }

    return store;
}
