import React, { PropTypes } from 'react';
import { I18n } from 'react-redux-i18n';
import Hand from '../components/Hand';
import { playRound } from '../actions/rounds';

const HandOptions = ({ dispatch }) => {
    return (<div>
        <h2>{I18n.t('application.chooseYourHand')}</h2>

        <button className="button-hand" onClick={() => { dispatch(playRound('Rock')); }}>
            <span className="hide-visually">{I18n.t('hands.rock')}</span>
            <Hand height="40" width="40" type="Rock" />
        </button>

        <button className="button-hand" onClick={() => { dispatch(playRound('Paper')); }}>
            <span className="hide-visually">{I18n.t('hands.paper')}</span>
            <Hand height="40" width="40" type="Paper" />
        </button>

        <button className="button-hand" onClick={() => { dispatch(playRound('Scissors')); }}>
            <span className="hide-visually">{I18n.t('hands.scissors')}</span>
            <Hand height="40" width="40" type="Scissors" />
        </button>

        <button className="button-hand" onClick={() => { dispatch(playRound('Lizard')); }}>
            <span className="hide-visually">{I18n.t('hands.lizard')}</span>
            <Hand height="40" width="40" type="Lizard" />
        </button>

        <button className="button-hand" onClick={() => { dispatch(playRound('Spock')); }}>
            <span className="hide-visually">{I18n.t('hands.spock')}</span>
            <Hand height="40" width="40" type="Spock" />
        </button>
    </div>);
};

HandOptions.propTypes = {
    dispatch: PropTypes.func.isRequired
};

export default HandOptions;
