import React, { PropTypes } from 'react';
import { I18n } from 'react-redux-i18n';

const Header = ({ round = 0, player1Score = 0, player2Score = 0 }) => {
    return (<header className="header">
        <h1 className="hide-visually">{I18n.t('application.title')}</h1>
        {I18n.t('application.round')} {round}
        <div className="header-score1">{I18n.t('application.player1Title')}:
            <span className="txt-emphasize"> {player1Score}</span>
        </div>
        <div className="header-score2">
            {I18n.t('application.player2Title')}:
            <span className="txt-emphasize"> {player2Score}</span>
        </div>
    </header>);
};

Header.propTypes = {
    round: PropTypes.number,
    player1Score: PropTypes.number,
    player2Score: PropTypes.number
};

export default Header;
