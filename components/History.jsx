import React, { PropTypes } from 'react';
import { I18n } from 'react-redux-i18n';
import toggleHistory from '../actions/game';
import Hand from './Hand';

const History = ({ dispatch, history = [], visible = false }) => {
    let historyButtonText = I18n.t('application.showHistory');
    if (visible) {
        historyButtonText = I18n.t('application.hideHistory');
    }

    return (<section>
        {history.length > 1 &&
            <button
                className="button-link"
                onClick={
                    () => {
                        dispatch(toggleHistory());
                    }
                }
            >
                {historyButtonText}
            </button>
        }
        {visible &&
            <ul className="history">
                {
                    history.reverse().map((historyDetails, index) => {
                        if (index !== 0) {
                            return (
                                <li className="history-row" key={`roundDetails${index}`}>
                                    {history.length - index}:
                                    <Hand
                                        height="20"
                                        width="20"
                                        type={historyDetails.player1hand}
                                    />
                                    <Hand
                                        height="20"
                                        width="20"
                                        type={historyDetails.player2hand}
                                    />.
                                    {I18n.t(`result.${historyDetails.outcome}`)}
                                </li>);
                        }
                        return (<noscript key={`roundDetails${index}`} />);
                    })
                }
            </ul>
        }
    </section>);
};

History.propTypes = {
    dispatch: PropTypes.func.isRequired,
    history: PropTypes.array,
    visible: PropTypes.bool
};

export default History;
