import React, { PropTypes } from 'react';
import { I18n } from 'react-redux-i18n';
import Hand from '../components/Hand';

const Result = ({ result, player1hand, player2hand }) => {
    if (result) {
        return (<section className="result">
            {(() => {
                switch (result) {
                    case 'player1win':
                        return <h2>{I18n.t(`result.${player1hand}${player2hand}`)}</h2>;
                    case 'player2win':
                        return <h2>{I18n.t(`result.${player2hand}${player1hand}`)}</h2>;
                    default:
                        return <h2>No winner this time</h2>;
                }
            })()}
            <p>{I18n.t(`result.${result}`)}</p>
            <Hand height="100" width="100" type={player1hand} />
            <Hand height="100" width="100" type={player2hand} />
        </section>);
    }
    return <noscript />;
};

Result.propTypes = {
    result: PropTypes.string,
    player1hand: PropTypes.string,
    player2hand: PropTypes.string
};

export default Result;
