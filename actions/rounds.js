import mod from '../utils/mod';

const possibleHands = ['Rock', 'Spock', 'Paper', 'Lizard', 'Scissors'];

export const randomAiHand = () => {
    return possibleHands[Math.floor(Math.random() * (possibleHands.length))];
};

export const checkWinner = (player1hand, player2hand) => {
    const player1handPos = possibleHands.indexOf(player1hand);
    const player2handPos = possibleHands.indexOf(player2hand);
    const result = {
        player1hand,
        player2hand
    };

    if (player1handPos === player2handPos) {
        result.outcome = 'draw';
    } else if (mod(
            (player1handPos - player2handPos), possibleHands.length
        ) <= possibleHands.length / 2) {
        result.outcome = 'player1win';
    } else {
        result.outcome = 'player2win';
    }

    return result;
};

export const player1SetHand = (hand) => {
    return {
        type: 'PLAYER1_SET_HAND',
        hand

    };
};

export const player2SetHand = (hand) => {
    return {
        type: 'PLAYER2_SET_HAND',
        hand

    };
};

export const playersDraw = (result) => {
    return {
        type: 'PLAYERS_DRAW',
        result
    };
};

export const player1win = (result) => {
    return {
        type: 'PLAYER1_WIN',
        result
    };
};

export const player2win = (result) => {
    return {
        type: 'PLAYER2_WIN',
        result
    };
};

export const playRound = (player1hand = randomAiHand()) => {
    const player2hand = randomAiHand();
    const result = checkWinner(player1hand, player2hand);

    return (dispatch) => {
        dispatch(player1SetHand(player1hand));

        dispatch(player2SetHand(player2hand));

        switch (result.outcome) {
            case 'draw':
                return dispatch(playersDraw(result));

            case 'player1win':
                return dispatch(player1win(result));

            default:
                return dispatch(player2win(result));
        }
    };
};
