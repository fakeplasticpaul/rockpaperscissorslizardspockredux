const toggleHistory = () => {
    return {
        type: 'TOGGLE_HISTORY'
    };
};

export default toggleHistory;

