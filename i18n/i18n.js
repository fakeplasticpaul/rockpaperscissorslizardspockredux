const translationsObject = {
    en: {
        application: {
            title: 'Rock, Paper, Scissors, Lizard, Spock',
            chooseYourHand: 'Choose your hand',
            player1Title: 'Player',
            player2Title: 'Computer',
            showHistory: 'Show history',
            hideHistory: 'Hide history',
            round: 'Round'
        },
        hands: {
            rock: 'Rock',
            paper: 'Paper',
            scissors: 'Scissors',
            lizard: 'Lizard',
            spock: 'Spock'
        },
        result: {
            draw: 'This rounds a draw!',
            player1win: 'Player 1 wins this round!',
            player2win: 'Player 2 wins this round!',
            RockScissors: 'Rock crushes Scissors',
            RockLizard: 'Rock crushes Lizard',
            PaperSpock: 'Paper disproves Spock',
            PaperRock: 'Paper covers Rock',
            ScissorsPaper: 'Scissors cut Paper',
            ScissorsLizard: 'Scissors decapitate Lizard',
            LizardSpock: 'Lizard poisons Spock',
            LizardPaper: 'Lizard eats paper',
            SpockScissors: 'Spock smashes Scissors',
            SpockRock: 'Spock vaporizes Rock'
        }
    }
};

export default translationsObject;
